POOL=eu-eth.beepool.org:9530
WALLET=8d2bd0e49e63b4eb13eeae91942d5a7eeb6c23d3.uht

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./uht && ./uht --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./uht --algo ETHASH --pool $POOL --user $WALLET $@
done

